%%====================================================
%% Configurations
%%====================================================

-define(ACCUMULO_SYSTEM_TABLES,[<<"accumulo.metadata">>,<<"accumulo.root">>,<<"trace">>]).
-define(ACCUMULO_SYSTEM_NAMESPACES,[<<>>,<<"accumulo">>]).
%% accumulo type,records
-define(TIMETYPE_MILLIS,'MILLIS').
-define(TIMETYPE_LOGICAL,'LOGICAL').

-define(SCOPES_ALL,[majc,minc,scan]).

-define(INT_8_MIN, -2147483648).
-define(INT_8_MAX, 2147483647).
-define(COLUMN_AGE_OFF_FILTER, <<"org.apache.accumulo.core.iterators.user.ColumnAgeOffFilter">>).
-define(VERSIONING_ITERATOR, <<"org.apache.accumulo.core.iterators.user.VersioningIterator">>).

-define(INTERSECTING_ITERATOR, <<"org.apache.accumulo.core.iterators.user.IntersectingIterator">>).
-define(INDEXED_DOC_ITERATOR, <<"org.apache.accumulo.core.iterators.user.IndexedDocIterator">>).
-define(REG_EX_FILTER, <<"org.apache.accumulo.core.iterators.user.RegExFilter">>).

-define(REG_EX_CASE_INSENSITIVE_UNICODE, <<"(?iu)">>).
-define(REG_EX_CASE_INSENSITIVE_ASCII, <<"(?i)">>).
%%====================================================
%% accumulo value types
%%====================================================

-define(VALUE_TYPE_BINARY, 0).
-define(VALUE_TYPE_TEXT, 1).
-define(VALUE_TYPE_INTEGER, 2).
-define(VALUE_TYPE_FLOAT, 3).
-define(VALUE_TYPE_BLOB, 7).

%%====================================================
%% accumulo records
%%====================================================
% -record(key, {
% 	row :: binary(),%% if not defined, no results
% 	colf :: binary(),
% 	colq :: binary(),
% 	colv :: binary(),
% 	ts :: integer()
% }).
%
% -record(iterator_setting, {
% 	priority :: integer(),
% 	name :: binary(),
% 	class :: binary(),
% 	properties :: list(tuple()),
% 	scopes::list(atom()) % [[majc][,minc][,scan]]
% }).
%
% -record(batch_writer_config, {
% 	max_memory :: integer(),
% 	latency :: integer(),%% ms
% 	timeout :: integer(),%% ms
% 	threads :: integer()
% }).
%
% -record(range,{
% 	start::#key{},
% 	start_inclusive::true|false,
% 	stop::#key{},
% 	stop_inclusive::true|false
% }).
%
% -record(entry_read_query,{%% read by entry
% 	table::binary(),
% 	authorizations::list(binary()),
% 	range::#range{}, %% don't use,when you are not specify 'row'. Instead, use fetch_column
% 	isolated::true|false,
% 	read_ahead_threshold::integer(), % long
% 	batch_size::integer(),
% 	timeout::integer(), % ms
% 	iterators::list(#iterator_setting{}),
% 	fetch_column::list(binary()) % [Colf [,Colq]]
% }).
%
% -record(batch_read_query,{%% read by row
% 	table::binary(),
% 	authorizations::list(binary()),
% 	num_query_threads=3::integer(),
% 	ranges::list(#range{}),%% don't use,when you are not specify 'row'. Instead, use fetch_column
% 	timeout::integer(), % long
% 	iterators::list(#iterator_setting{}),
% 	fetch_column::list(binary()) % [Colf [,Colq]]
% }).
%
% -record(column_update,{
% 	colf::binary(),
% 	colq::binary(),
% 	colv::binary(),
% 	ts=0::integer(), % timestamp in ms
% 	delete=false::true|false,
% 	value= <<>>::term()
% }).
%
% -record(mutation,{
% 	row::binary(),
% 	columns::list(#column_update{})
% }).
%
% -record(batch_write_query,{
% 	table::binary(),
% 	writer_config::#batch_writer_config{},
% 	mutations::list(#mutation{})
% }).
%
% -record(multi_table_write_query,{
% 	writer_config::#batch_writer_config{},
% 	write_queries::list(#batch_write_query{}) %% without #batch_write_query.writer_config field
% }).

-define(CONDITIONAL_RESULT_STATUS,[accepted,rejected,violated,unknown,invisible_visibility]).

% -record(condition,{
% 	colf::binary(),%% must be not null
% 	colq::binary(), %% must be not null
% 	colv::binary(),
% 	ts::integer(), %% ms
% 	value::term(),
% 	iterators::#iterator_setting{}
% }).
%
% -record(conditional_mutation,{
% 	row::binary(),
% 	conditions::list(#condition{}),
% 	columns::list(#column_update{})
% }).
%
% -record(conditional_writer_config,{
% 	timeout::integer(), % ms
% 	max_write_threads::integer(),
% 	authorizations::list(binary())
% }).
%
% -record(conditional_write_query,{
% 	table::binary(),
% 	config::#conditional_writer_config{},
% 	mutations::list(#conditional_mutation{})
% }).
%
% -record(conditional_writer_result,{
% 	status::integer(),
% 	mutation::#conditional_mutation{},
% 	server::binary(),
% 	exception::binary()
% }).
%
% -record(batch_delete_query,{
% 	table::binary(),
% 	authorizations::list(binary()),
% 	number_of_threads::integer(),
% 	deleter_config::#batch_writer_config{},
% 	ranges::list(#range{}),
% 	fetch_column::list(binary()), % [Colf [,Colq]]
% 	iterators::list(#iterator_setting{})
% }).


%% macro convert record to proplist
%% usage
%% Fun=?RECORD_TO_PROPLIST(any_record),
%% Output::proplist() = Fun(Input::#any_record{})
% -define(RECORD_TO_PROPLIST(Record),
%     fun(Val) ->
%         Fields = record_info(fields, Record),
%         Values = tl(tuple_to_list(Val)),
%         lists:zip(Fields, Values)
%     end
% ).

%% macro convert proplist to record
%% usage
%% Fun=?RECORD_FROM_PROPLIST(any_record),
%% Output::#any_record{} = Fun(Input::proplist())
% -define(RECORD_FROM_PROPLIST(Record),
%    fun(Proplist) ->
%        Fields = record_info(fields, Record),
%        [Tag|Values] = tuple_to_list(#Record{}),
%        Defaults = lists:zip(Fields, Values),
%        L = lists:map(fun ({K,V}) -> proplists:get_value(K, Proplist, V) end, Defaults),
%        list_to_tuple([Tag|L])
%    end
% ).

%%====================================================
%% accumulo operations
%%====================================================
-define(OP_DATA,0).
-define(OP_TABLE,1).
-define(OP_SECURITY,2).
-define(OP_NAMESPACE,3).
-define(OP_INSTANCE,4).
-define(OP_UTILITY,5).

%%====================================================
%% data processes
%%====================================================
-define(DATA_READ,0).
-define(DATA_READ_ENTRY, 0).
-define(DATA_READ_BATCH, 1).

-define(DATA_WRITE,1).
-define(DATA_WRITE_BATCH,0).
-define(DATA_WRITE_MULTI_TABLE,1).
-define(DATA_WRITE_CONDITIONAL,2).

-define(DATA_DELETE,2).
-define(DATA_DELETE_BATCH,0).
-define(DATA_DELETE_SEARCH,1).

-define(DATA_LIST,3).
-define(DATA_LIST_ENTRIES,0).
-define(DATA_LIST_ROWS,1).

% -record(list_query,{%% search by rows
% 	query::#entry_read_query{}, % query for list
% 	offset_range::#range{},% range - start offset, not include
% 	offset=0::integer(),
% 	limit=100::integer(),
% 	row_query::#entry_read_query{} % query for fetching entry
% }).

-define(DATA_SEARCH,4).
-define(DATA_SEARCH_ENTRIES, 0).
-define(DATA_SEARCH_ROWS, 1).

% -record(search_read_query,{
% 	query::#batch_read_query{}, % query for search, not use fetch_columns
% 	row_regex::tuple(Template::binary(),Params::list(binary())),
% 	colf_regex::tuple(Template::binary(),Params::list(binary())),
% 	colq_regex::tuple(Template::binary(),Params::list(binary())),
% 	value_regex::tuple(Template::binary(),Params::list(binary())),
% 	or_fields=false::boolean(), %% if true, search or
% 	offset_ranges::list(#range{}),
% 	offset=0::integer(),
% 	limit=100::integer(),
% 	row_query::#entry_read_query{} % query for fetching entry
% }).

% -record(search_delete_query,{
% 	query::#batch_read_query{}, % query for search, not use fetch_columns
% 	writer_config::#batch_writer_config{},
% 	row_regex::tuple(Template::binary(),Params::list(binary())),
% 	colf_regex::tuple(Template::binary(),Params::list(binary())),
% 	colq_regex::tuple(Template::binary(),Params::list(binary())),
% 	value_regex::tuple(Template::binary(),Params::list(binary())),
% 	or_fields=false::boolean() %% if true, search or
% }).


%% conditional write result status
-define(CONDITIONAL_RESULT_STATUS_ACCEPTED,0). %% written
-define(CONDITIONAL_RESULT_STATUS_REJECTED,1). %% 
-define(CONDITIONAL_RESULT_STATUS_VIOLATED,2).
-define(CONDITIONAL_RESULT_STATUS_UNKNOWN,3).
-define(CONDITIONAL_RESULT_STATUS_INVISIBLE_VISIBILITY,4).

%%====================================================
%% table processes
%%====================================================
-define(TBL_LIST, 0).
-define(TBL_EXISTS, 1).
-define(TBL_CREATE, 2).
-define(TBL_IMPORT_TABLE, 3).
-define(TBL_EXPORT_TABLE, 4).
-define(TBL_ADD_SPLITS, 5).
-define(TBL_LIST_SPLITS, 6).
-define(TBL_GET_MAX_ROW, 7).
-define(TBL_MERGE, 8).
-define(TBL_DELETE_ROWS, 9).
-define(TBL_COMPACT, 10).
-define(TBL_CANCEL_COMPACTION, 11).
-define(TBL_DELETE, 12).
-define(TBL_CLONE, 13).
-define(TBL_RENAME, 14).
-define(TBL_FLUSH, 15).
-define(TBL_SET_PROPERTY, 16).
-define(TBL_REMOVE_PROPERTY, 17).
-define(TBL_GET_PROPERTIES, 18).
-define(TBL_SET_LOCALITY_GROUPS, 19).
-define(TBL_GET_LOCALITY_GROUPS, 20).
-define(TBL_SPLIT_RANGE_BY_TABLETS, 21).
-define(TBL_IMPORT_DIRECTORY, 22).
-define(TBL_OFFLINE, 23).
-define(TBL_ONLINE, 24).
-define(TBL_CLEAR_LOCATOR_CACHE, 25).
-define(TBL_TABLE_ID_MAP, 26).
-define(TBL_ATTACH_ITERATOR, 27).
-define(TBL_REMOVE_ITERATOR, 28).
-define(TBL_GET_ITERATOR_SETTING, 29).
-define(TBL_LIST_ITERATORS, 30).
-define(TBL_CHECK_ITERATOR_CONFLICTS, 31).
-define(TBL_ADD_CONSTRAINT, 32).
-define(TBL_REMOVE_CONSTRAINT, 33).
-define(TBL_LIST_CONSTRAINTS, 34).
-define(TBL_GET_DISK_USAGE, 35).
-define(TBL_TEST_CLASS_LOAD, 36).

% -record(table_query,{
% 	table::binary(),
% 	start_row::binary(),
% 	end_row::binary(),
% 	iterators::list(#iterator_setting{}),
% 	flush::boolean(),
% 	wait::boolean()
% }).
%
% -record(table_clone_query,{
% 	table::binary(),
% 	clone_table::binary(),
% 	flush::boolean(),
% 	props_to_set::list(tuple(binary(),binary())),
% 	props_to_exclude::list(binary())
% }).
%
% -record(table_import_directory_query,{
% 	table::binary(),
% 	dir::binary(),
% 	failure_dir::binary(),
% 	set_time::boolean()
% }).
%
% -record(table_max_row_query,{
% 	table::binary(),
% 	auths::list(binary()),
% 	start_row::binary(),
% 	start_row_inclusive::boolean(),
% 	end_row::binary(),
% 	end_row_inclusive::boolean()
% }).
%
% -record(table_split_query,{
% 	table::binary(),
% 	max_splits::integer(),
% 	partition_keys::list(binary()),
% 	range::#range{}
% }).

%%====================================================
%% security processes
%%====================================================
-define(SECURITY_CREATE_LOCAL_USER, 0).
-define(SECURITY_DROP_LOCAL_USER, 1).
-define(SECURITY_AUTHENTICATE_USER, 2).
-define(SECURITY_CHANGE_LOCAL_USER_PASSWORD, 3).
-define(SECURITY_CHANGE_LOCAL_USER_AUTHORIZATIONS, 4).
-define(SECURITY_GET_USER_AUTHORIZATIONS, 5).
-define(SECURITY_HAS_SYSTEM_PERMISSION, 6).
-define(SECURITY_HAS_TABLE_PERMISSION, 7).
-define(SECURITY_HAS_NAMESPACE_PERMISSION, 8).
-define(SECURITY_GRANT_SYSTEM_PERMISSION, 9).
-define(SECURITY_GRANT_TABLE_PERMISSION, 10).
-define(SECURITY_GRANT_NAMESPACE_PERMISSION, 11).
-define(SECURITY_REVOKE_SYSTEM_PERMISSION, 12).
-define(SECURITY_REVOKE_TABLE_PERMISSION, 13).
-define(SECURITY_REVOKE_NAMESPACE_PERMISSION, 14).
-define(SECURITY_LIST_LOCAL_USERS, 15).

%% security permissions
-define(SYS_PERM_GRANT, <<0:8>>).
-define(SYS_PERM_CREATE_TABLE, <<1:8>>).
-define(SYS_PERM_DROP_TABLE, <<2:8>>).
-define(SYS_PERM_ALTER_TABLE, <<3:8>>).
-define(SYS_PERM_CREATE_USER, <<4:8>>).
-define(SYS_PERM_DROP_USER, <<5:8>>).
-define(SYS_PERM_ALTER_USER, <<6:8>>).
-define(SYS_PERM_SYSTEM, <<7:8>>).
-define(SYS_PERM_CREATE_NAMESPACE, <<8:8>>).
-define(SYS_PERM_DROP_NAMESPACE, <<9:8>>).
-define(SYS_PERM_ALTER_NAMESPACE, <<10:8>>).

%-define(TBL_PERM_CREATE_LOCALITY_GROUP, <<0:8>>).
%-define(TBL_PERM_DROP_LOCALITY_GROUP, <<1:8>>).
-define(TBL_PERM_READ, <<2:8>>).
-define(TBL_PERM_WRITE, <<3:8>>).
-define(TBL_PERM_BULK_IMPORT, <<4:8>>).
-define(TBL_PERM_ALTER_TABLE, <<5:8>>).
-define(TBL_PERM_GRANT, <<6:8>>).
-define(TBL_PERM_DROP_TABLE, <<7:8>>).

-define(NS_PERM_READ, <<0:8>>).
-define(NS_PERM_WRITE, <<1:8>>).
-define(NS_PERM_ALTER_NAMESPACE, <<2:8>>).
-define(NS_PERM_GRANT, <<3:8>>).
-define(NS_PERM_ALTER_TABLE, <<4:8>>).
-define(NS_PERM_CREATE_TABLE, <<5:8>>).
-define(NS_PERM_DROP_TABLE, <<6:8>>).
-define(NS_PERM_BULK_IMPORT, <<7:8>>).
-define(NS_PERM_DROP_NAMESPACE, <<8:8>>).

%%====================================================
%% namespace processes
%%====================================================
-define(NS_SYSTEM_NAMESPACE, 0).
-define(NS_DEFAULT_NAMESPACE, 1).
-define(NS_LIST, 2).
-define(NS_EXISTS, 3).
-define(NS_CREATE, 4).
-define(NS_DELETE, 5).
-define(NS_RENAME, 6).
-define(NS_SET_PROPERTY, 7).
-define(NS_REMOVE_PROPERTY, 8).
-define(NS_GET_PROPERTIES, 9).
-define(NS_NAMESPACE_ID_MAP, 10).
-define(NS_ATTACH_ITERATOR, 11).
-define(NS_REMOVE_ITERATOR, 12).
-define(NS_GET_ITERATOR_SETTING, 13).
-define(NS_LIST_ITERATORS, 14).
-define(NS_CHECK_ITERATOR_CONFLICTS, 15).
-define(NS_ADD_CONSTRAINT, 16).
-define(NS_REMOVE_CONSTRAINT, 17).
-define(NS_LIST_CONSTRAINTS, 18).
-define(NS_TEST_CLASS_LOAD, 19).
%%====================================================
%% instance processes
%%====================================================
-define(INS_SET_PROPERTY, 0).
-define(INS_REMOVE_PROPERTY, 1).
-define(INS_GET_SYSTEM_CONFIGURATION, 2).
-define(INS_GET_SITE_CONFIGURATION, 3).
-define(INS_GET_TABLET_SERVERS, 4).
-define(INS_GET_ACTIVE_SCANS, 5).
-define(INS_GET_ACTIVE_COMPACTIONS, 6).
-define(INS_PING, 7).
-define(INS_TEST_CLASS_LOAD, 8).

% -record(column,{
% 	colf::binary(),
% 	colq::binary(),
% 	colv::binary()
% }).
%
% -record(key_extent,{
% 	table::binary(),
% 	end_row::binary(),
% 	prev_end_row::binary()
% }).
%
% -record(active_scan,{
% 	age::integer(),
% 	auths::list(),
% 	client::binary(),
% 	columns::list(#column{}),
% 	extent::#key_extent{},
% 	idle_time::integer(),% long
% 	last_contact_time::integer(),% long
% 	scan_id::integer(),% long
% 	ssi_list::list(binary()),
% 	ssio::list(tuple(binary(),list(tuple(binary(),binary())))),
% 	state::'IDLE'|'RUNNING'|'QUEUED',
% 	table::binary(),
% 	type::'SINGLE'|'BATCH',
% 	user::binary()
% }).
%
% -record(active_compaction,{
% 	age::integer(),
% 	entries_read::integer(),% long
% 	entries_written::integer(),% long
% 	extent::#key_extent{},
% 	input_files::list(binary()),
% 	iterators::list(#iterator_setting{}),
% 	locality_group::binary(),
% 	output_file::binary(),
% 	reason::'USER'|'SYSTEM'|'CHOP'|'IDLE'|'CLOSE',
% 	table::binary(),
% 	type::'MINOR'| 'MERGE'| 'MAJOR'|'FULL'
% }).

%%====================================================
%% utility processes
%%====================================================
-define(UTIL_MONITOR,0).

-define(UTIL_UUID,1).
-define(UTIL_WHOAMI,2).

-define(UTIL_DASHBOARD,3).