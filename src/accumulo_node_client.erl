-module(accumulo_node_client).

%% accumulo_node_client: accumulo_node_client library's entry point.

-export([setup/2, query/1]).

setup(NodeSName,NodeName) ->
	accumulo_util:ets_create(),
	accumulo_util:ets_put({node_sname,NodeSName}),
	accumulo_util:ets_put({node_name,NodeName}).
	
query(Params) ->
	{accumulo_util:ets_get(node_sname), accumulo_util:ets_get(node_name)}!{self(),Params},
	receive
		Reply ->
			Reply
	end.
