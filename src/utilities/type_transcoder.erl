-module(type_transcoder).

% -include("accumulo.hrl").

-export([encode/2, decode/1, decode_integer/3]).
-export([reverse/2]).

%%===============================================================
%% data encode from binary
%%===============================================================
% encode(flag, << Flag:8 >>) ->
% 	Flag;
%
encode(integer, IntBin) ->
	BitSize=bit_size(IntBin),
	<<Int:BitSize/signed-integer>> = IntBin,
	Int;
	
encode(float, << Float/float >>) ->
	Float;

% encode(boolean, << Boolean:8 >> ) ->
% 	Boolean == 1;
%
encode(_,_)->
	bad_request.
	
%%===============================================================
%% data decode to binary
%%===============================================================
% decode(flag, Flag) ->
% 	<< Flag:8 >>;
%
decode(Value) when is_binary(Value) ->
	Value;
decode(Value) when is_integer(Value)->
	decode_integer(Value, << Value:64/signed-integer >>, 64);
	
decode(Value) when is_float(Value)->
	<< Value/float >>;
	
decode(Value) when is_boolean(Value) ->
	case Value of
		true ->
			<< 1:8 >>;
		false ->
			<< 0:8 >>
	end;
decode(Value) when is_atom(Value) ->
	atom_to_binary(Value,utf8);
	
decode(_) ->
	bad_request.

decode_integer(Value, Bin , Size) ->
	 << BinValue:Size/signed-integer >> = Bin,
	case Value =:= BinValue of
		true ->
			Bin;
		false ->
			NextSize = Size+8,
			decode_integer(Value, << Value:NextSize/signed-integer >>, NextSize)
	end.
			

%% TextBin << "..."/utf8 >>
reverse(text, TextBin) ->
	unicode:characters_to_binary(lists:reverse(unicode:characters_to_list(TextBin,utf8)),utf8);
	
reverse(_, Bin) when is_binary(Bin) ->
    S = bit_size(Bin),
    << V:S/integer-little>> = Bin,
    << V:S/integer-big>>;

reverse(_,_) ->
	bad_args.
