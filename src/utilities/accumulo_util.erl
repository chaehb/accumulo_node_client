-module(accumulo_util).

-author('Chae, H B <chaehb@gmail.com>').

-include("accumulo.hrl").

-export([ets_create/0, ets_get/1, ets_get_table/2, ets_put/1]).
-export([strip_map/1,strip_map/2]).
-export([gen_commands/2,gen_commands/3]).

-define(ETS_TABLE,accumulo_setup).

%%===============================
%%
%%	API
%%
%%===============================
ets_create() ->
	case ets:info(?ETS_TABLE,name) of
		undefined ->
			ets:new(?ETS_TABLE,[set,named_table]);
		Name ->
			Name
	end.

ets_get_table(N,Key) ->
	lists:nth(N,ets_get(Key)).
	
ets_get(Key) ->
	try
		[{Key,Value}] = ets:lookup(?ETS_TABLE,Key),
		Value
	catch
		_:_ ->
			undefined
	end.

ets_put(KeyValue) ->
	ets:insert(?ETS_TABLE,KeyValue).

strip_map(Map) ->
	maps:fold(
		fun(Key,Value,NewMap) -> 
			case Value of
				null ->
					NewMap;
				_ ->
					maps:put(Key,Value,NewMap)
			end
		end,
		maps:new(),
		Map
	).
	
strip_map(Map,Filter) ->
	maps:fold(
		fun(Key,Value,NewMap) -> 
			case Value of
				Filter ->
					NewMap;
				_ ->
					maps:put(Key,Value,NewMap)
			end
		end,
		maps:new(),
		Map
	).
	
gen_commands(Op,Proc) ->
	#{op => Op, proc => Proc}.
	
gen_commands(Op,Proc,Action) ->
	#{op => Op, proc => Proc, action => Action}.
