-module(accumulo_security).

-include("accumulo.hrl").

-export([list_local_users/0]).
-export([create_local_user/2,drop_local_user/1]).
-export([authenticate_user/2,change_local_user_password/2]).
-export([change_local_user_authorizations/2, get_user_authorizations/1, add_user_authorizations/2]).
-export([has_system_permission/2,has_table_permission/3,has_namespace_permission/3]).
-export([grant_system_permission/2,grant_table_permission/3,grant_namespace_permission/3]).
-export([revoke_system_permission/2,revoke_table_permission/3,revoke_namespace_permission/3]).

%% out : {ok,list(User::binary())} | {error,Message}
list_local_users() ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_LIST_LOCAL_USERS),
	accumulo_node_client:query({Commands}).
		
% in : UserID::binary(), Password::binary()
%% out : {ok, true|false} | {error, Message}
create_local_user(UserID,Password) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_CREATE_LOCAL_USER),
	Options = #{ userid => UserID, password => Password},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary()
%% out : {ok, true|false} | {error,Message}		
drop_local_user(UserID) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_DROP_LOCAL_USER),
	Options = #{userid => UserID},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), Password::binary()
%% out : {ok, true|false} | {error, Message}
authenticate_user(UserID,Password) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_AUTHENTICATE_USER),
	Options = #{userid => UserID, password => Password},
	accumulo_node_client:query({Commands, Options}).
	
% in : UserID::binary(), Password::binary()
%% out : {ok, true|false} | {error, Message}
change_local_user_password(UserID,Password) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_CHANGE_LOCAL_USER_PASSWORD),
	Options = #{userid => UserID, password => Password},
	accumulo_node_client:query({Commands, Options}).
	
% in : UserID::binary(), Auths::list(Auth::binary())
%% out : {ok, true|false} | {error, Message}
change_local_user_authorizations(UserID,Auths) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_CHANGE_LOCAL_USER_AUTHORIZATIONS),
	Options = #{userid => UserID, authorizations => Auths},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary()
%% out : {ok,list(Auth::binary())} | {error,Message}
get_user_authorizations(UserID) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_GET_USER_AUTHORIZATIONS),
	Options = #{userid => UserID},
	accumulo_node_client:query({Commands, Options}).
	
% in : UserID::binary(), Auths::list(Auth::binary())
%% out : {ok, true|false} | {error, Message}
add_user_authorizations(UserID,Auths) ->
	case get_user_authorizations(UserID) of
		{ok, OldAuths} ->
			change_local_user_authorizations(UserID, lists:usort(OldAuths ++ Auths));
		_ ->
			{error, bad_request}
	end.

% in : UserID::binary(), SystemPerm::flag()
%% out : {ok, true|false} | {error, Message}
has_system_permission(UserID, SystemPerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_HAS_SYSTEM_PERMISSION),
	Options = #{userid => UserID, system_perm => SystemPerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), TableName::binary(), TablePerm::flag()
%% out : {ok, true|false} | {error, Message}
has_table_permission(UserID, TableName, TablePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_HAS_TABLE_PERMISSION),
	Options = #{userid => UserID, table => TableName, table_perm => TablePerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), Namespace::binary(), NamespacePerm::flag()
%% out : {ok, true|false} | {error, Message}
has_namespace_permission(UserID, Namespace, NamespacePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_HAS_NAMESPACE_PERMISSION),
	Options = #{userid => UserID, namespace => Namespace, namespace_perm => NamespacePerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), SystemPerm::flag()
%% out : {ok, true|false} | {error, Message}
grant_system_permission(UserID, SystemPerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_GRANT_SYSTEM_PERMISSION),
	Options = #{userid => UserID, system_perm => SystemPerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), TableName::binary(), TablePerm::flag()
%% out : {ok, true|false} | {error, Message}
grant_table_permission(UserID, TableName, TablePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_GRANT_TABLE_PERMISSION),
	Options = #{userid => UserID, table => TableName, table_perm => TablePerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), Namespace::binary(), NamespacePerm::flag()
%% out : {ok, true|false} | {error, Message}
grant_namespace_permission(UserID, Namespace, NamespacePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_GRANT_NAMESPACE_PERMISSION),
	Options = #{userid => UserID, namespace => Namespace, namespace_perm => NamespacePerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), SystemPerm::flag()
%% out : {ok, true|false} | {error, Message}
revoke_system_permission(UserID, SystemPerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_REVOKE_SYSTEM_PERMISSION),
	Options = #{userid => UserID, system_perm => SystemPerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), TableName::binary(), TablePerm::flag()
%% out : {ok, true|false} | {error, Message}
revoke_table_permission(UserID, TableName, TablePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_REVOKE_TABLE_PERMISSION),
	Options = #{userid => UserID, table => TableName, table_perm => TablePerm},
	accumulo_node_client:query({Commands, Options}).

% in : UserID::binary(), Namespace::binary(), NamespacePerm::flag()
%% out : {ok, true|false} | {error, Message}
revoke_namespace_permission(UserID, Namespace, NamespacePerm) ->
	Commands = accumulo_util:gen_commands(?OP_SECURITY,?SECURITY_REVOKE_NAMESPACE_PERMISSION),
	Options = #{userid => UserID, namespace => Namespace, namespace_perm => NamespacePerm},
	accumulo_node_client:query({Commands, Options}).


	