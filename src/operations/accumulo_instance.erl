-module(accumulo_instance).

-include("accumulo.hrl").

-export([set_property/2,remove_property/1]).
-export([get_system_configuration/0,get_site_configuration/0]).
-export([get_tablet_servers/0,get_active_scans/1,get_active_compactions/1]).
-export([ping/1,test_class_load/2]).

% in : Property::binary(),Value::binary()
% out : {ok,set_property} | {error,Message}
set_property(Property,Value) ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_SET_PROPERTY),
	Options =#{property =>Property, value => Value},
	accumulo_node_client:query({Commands, Options}).

% in : Property::binary()
% out : {ok,remove_property} | {error,Message}
remove_property(Property) ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_REMOVE_PROPERTY),
	Options = #{property => Property},
	accumulo_node_client:query({Commands, Options}).

%% out : {ok,list(tuple{Property::binary(),Value::binary()})} | {error,Message}
get_system_configuration() ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_GET_SYSTEM_CONFIGURATION),
	accumulo_node_client:query({Commands}).

%% out : {ok,list(tuple{Property::binary(),Value::binary()})} | {error,Message}
get_site_configuration() ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_GET_SITE_CONFIGURATION),
	accumulo_node_client:query({Commands}).

%% out : {ok,list(TServer::binary())} | {error,Message}
get_tablet_servers() ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_GET_TABLET_SERVERS),
	accumulo_node_client:query({Commands}).

% in : TServer::binary()
%% out : {ok, list(#active_scan{})} | {error,Message}
get_active_scans(TServer) ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_GET_ACTIVE_SCANS),
	Options = #{tserver => TServer},
	accumulo_node_client:query({Commands, Options}).

% in : TServer::binary()
%% out : {ok, list(#active_compaction{})} | {error,Message}
get_active_compactions(TServer) ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_GET_ACTIVE_COMPACTIONS),
	Options = #{tserver => TServer},
	accumulo_node_client:query({Commands, Options}).

% in : TServer::binary()
%% out : {ok, ping|pong} | {error,Message}
ping(TServer) ->
	Commands = accumulo_util:gen_commands(?OP_INSTANCE,?INS_PING),
	Options = #{tserver => TServer},
	accumulo_node_client:query({Commands, Options}).

test_class_load(_Class,_AsType) ->
	{error, not_implemented}.
