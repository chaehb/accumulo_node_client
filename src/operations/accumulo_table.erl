%%
%% before send records, they should be decoded to proplists
%%
-module(accumulo_table).

-include("accumulo.hrl").

-export([
	list/0, exists/1, create/3, rename/2, delete/1,
	flush/3, flush/4, compact/4,compact/5,compact/6,cancel_compaction/1, clone/5,
	import_table/2, export_table/2, import_directory/3,import_directory/4,
	offline/1, offline/2, online/1, online/2,
	clear_locator_cache/1, table_id_map/0,
	get_properties/1, set_property/2, remove_property/2
]).
-export([delete_rows/3, merge/3, get_max_row/4, get_max_row/6]).

-export([set_locality_groups/2, get_locality_groups/1]).
	
-export([
	list_iterators/1, attach_iterator/2, attach_iterator/3,
	get_iterator_setting/3, remove_iterator/3, 
	check_iterator_conflicts/3
]).
	
-export([list_constraints/1, add_constraint/2, remove_constraint/2]).
	
-export([list_splits/1, list_splits/2, add_splits/2, split_range_by_tablets/1, split_range_by_tablets/3]).

-export([disk_usage/1]).


%%
list() ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_LIST),
	accumulo_node_client:query({Commands}).
%%	{ok,list(binary())}
%%	{error, Error}
	
%% Tablename::binary()
exists(Tablename) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_EXISTS),
	Options = #{table => Tablename},
	accumulo_node_client:query({Commands, Options}).
%% response
%%	{ok,true|false}
%%	{error, Error}

%% Tablename::binary(),Versioning::true|falsw, TimeType::'MILLIS'|'LOGICAL'
create(Tablename,Versioning, TimeType) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_CREATE),
	Options = #{table => Tablename, versioning => Versioning, timetype => TimeType},
	accumulo_node_client:query({Commands, Options}).
%%	{ok,create}
%%	{error,table_exists | Error}

%% Tablename::binary(),NewName::binary()
rename(Tablename, NewName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_RENAME),
	Options = #{table => Tablename, new_table => NewName},
	accumulo_node_client:query({Commands,Options}).
%%	{ok,rename}
%%	{error, table_not_found | table_exists | Error}
			
%% Tablename::binary()
delete(Tablename) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_DELETE),
	Options = #{table => Tablename},
	accumulo_node_client:query({Commands,Options}).
%%	{ok,delete}
%%	{error, table_not_found | Error}

% -define(TBL_FLUSH, 15).
%% #table_query{table,start_row,end_row,wait}
flush(Table,StartRow,EndRow) ->
	flush(Table,StartRow,EndRow,false).
	
flush(Table,StartRow,EndRow,Wait) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_FLUSH),
	Options0 = #{table => Table, wait => Wait },
	Options1 = case StartRow of
		null ->
			Options0 ;
		_ ->
			maps:put(start_row,StartRow,Options0)
	end,
	Options = case EndRow of
		null ->
			Options1 ;
		_ ->
			maps:put(end_row,EndRow,Options1)
	end,

	accumulo_node_client:query({Commands, Options}).
	
% -define(TBL_COMPACT, 10).
%% #table_query{table,start_row,end_row,iterators,flush,wait}
compact(Table,StartRow,EndRow,Iterators) ->
	compact(Table,StartRow,EndRow,Iterators,true,false).
	
compact(Table,StartRow,EndRow,Iterators,Flush) ->
	compact(Table,StartRow,EndRow,Iterators,Flush,false).
	
compact(Table,StartRow,EndRow,Iterators,Flush,Wait) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_COMPACT),

	Options0 = #{table => Table, flush => Flush, wait => Wait },
	Options1 = case StartRow of
		null ->
			Options0 ;
		_ ->
			maps:put(start_row,StartRow,Options0)
	end,
	Options2 = case EndRow of
		null ->
			Options1 ;
		_ ->
			maps:put(end_row,EndRow,Options1)
	end,

	Options = case Iterators of
		null ->
			Options2 ;
		_ ->
			maps:put(iterators,Iterators,Options2)
	end,

	accumulo_node_client:query({Commands, Options}).

% -define(TBL_CANCEL_COMPACTION, 11).
cancel_compaction(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_CANCEL_COMPACTION),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands, Options}).

% -define(TBL_CLONE, 13).
clone(TableName,CloneTableName,Flush,PropertiesToSet,PropertiesToExclude) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_CLONE),
	Options0 = #{table => TableName, clone_table => CloneTableName, flush => Flush},
	Options1 = case PropertiesToSet of
		null ->
			Options0;
		_ ->
			maps:put(properties_to_set,PropertiesToSet,Options0)
	end,
	
	Options = case PropertiesToExclude of
		null ->
			Options1;
		_ ->
			maps:put(properties_to_exclude,PropertiesToExclude,Options1)
	end,
	
	accumulo_node_client:query({Commands, Options}).
	
% -define(TBL_IMPORT_TABLE, 3).
import_table(TableName,ImportDir) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_IMPORT_TABLE),
	Options = #{table => TableName, import_dir => ImportDir},
	accumulo_node_client:query({Commands, Options}).
		
% -define(TBL_EXPORT_TABLE, 4).
export_table(TableName,ExportDir) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_EXPORT_TABLE),
	Options = #{table => TableName, export_dir => ExportDir},
	accumulo_node_client:query({Commands, Options}).

% -define(TBL_IMPORT_DIRECTORY, 22).
import_directory(TableName,Directory,FailureDirectory) ->
	import_directory(TableName,Directory,FailureDirectory,false).
	
import_directory(TableName,Directory,FailureDirectory,SetTime) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_IMPORT_DIRECTORY),
	Options = #{
		table => TableName,
		dir => Directory,
		failure_dir => FailureDirectory,
		set_time => SetTime
	},
	accumulo_node_client:query({Commands, Options}).
		
% -define(TBL_OFFLINE, 23).
offline(TableName) ->
	offline(TableName,false).
	
offline(TableName,Wait) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_OFFLINE),
	Options = #{table => TableName, wait => Wait},
	accumulo_node_client:query({Commands, Options}).
	
% -define(TBL_ONLINE, 24).
online(TableName) ->
	online(TableName,false).
online(TableName,Wait) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_ONLINE),
	Options = #{table => TableName, wait => Wait},
	accumulo_node_client:query({Commands, Options}).

% -define(TBL_CLEAR_LOCATOR_CACHE, 25).
clear_locator_cache(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_CLEAR_LOCATOR_CACHE),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands, Options}).

% -define(TBL_TABLE_ID_MAP, 26).
%% out : list(tuple(binary(),binary()))
table_id_map() ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_TABLE_ID_MAP),
	accumulo_node_client:query({Commands}).

% -define(TBL_GET_PROPERTIES, 18).
%% out : list(tuple(binary(),binary()))
get_properties(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_GET_PROPERTIES),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_SET_PROPERTY, 16).
set_property(TableName,{Property,Value}) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_SET_PROPERTY),
	Options = #{table => TableName, property => Property, value => Value},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_REMOVE_PROPERTY, 17).
remove_property(TableName,Property) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_REMOVE_PROPERTY),
	Options = #{table => TableName, property => Property},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_DELETE_ROWS, 9).
% #table_query{table,start_row,end_row}
delete_rows(TableName,StartRow,EndRow) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_DELETE_ROWS),
	Options0 = #{table => TableName},
	Options1 = case StartRow of
		null ->
			Options0;
		_ ->
			maps:put(start_row,StartRow,Options0)
	end,
	Options = case EndRow of
		null ->
			Options1;
		_ ->
			maps:put(end_row,EndRow,Options1)
	end,
	accumulo_node_client:query({Commands, Options}).
		
% -define(TBL_MERGE, 8).
% #table_query{table,start_row,end_row}
merge(TableName,StartRow,EndRow) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_MERGE),
	Options0 = #{table => TableName},
	Options1 = case StartRow of
		null ->
			Options0;
		_ ->
			maps:put(start_row,StartRow,Options0)
	end,
	Options = case EndRow of
		null ->
			Options1;
		_ ->
			maps:put(end_row,EndRow,Options1)
	end,
	accumulo_node_client:query({Commands, Options}).
	
% -define(TBL_GET_MAX_ROW, 7).
%% {ok,MaxRow::binary()} | {error,Message}
get_max_row(TableName,Auths,StartRow,EndRow) ->
	get_max_row(TableName,Auths,StartRow,true,EndRow,true).
	
get_max_row(TableName,Auths,StartRow,StartInclusive,EndRow,EndInclusive) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE, ?TBL_GET_MAX_ROW),
	Options0 = #{table => TableName, auths => Auths, start_inclusive => StartInclusive, end_inclusive => EndInclusive},
	Options1 = case StartRow of
		null ->
			Options0;
		_ ->
			maps:put(start_row,StartRow,Options0)
	end,
	Options = case EndRow of
		null ->
			Options1;
		_ ->
			maps:put(end_row,EndRow,Options1)
	end,
	accumulo_node_client:query({Commands, Options}).


% -define(TBL_SET_LOCALITY_GROUPS, 19).
%% input : Tablename::binary(), LocalityGroups::list(tuple(binary(),list(binary())))
%% output : {ok|error,atom()}
set_locality_groups(Tablename, LocalityGroups) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_SET_LOCALITY_GROUPS),
	Options = #{table => Tablename, groups => LocalityGroups},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_GET_LOCALITY_GROUPS, 20).
%% input tablename
%% output {ok,LocalityGroups::list(tuple(binary(),list(binary())))}|{error,Message}
get_locality_groups(Tablename) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_GET_LOCALITY_GROUPS),
	Options = #{table => Tablename},
	accumulo_node_client:query({Commands,Options}).


% -define(TBL_LIST_ITERATORS, 30).
list_iterators(Tablename) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_LIST_ITERATORS),
	accumulo_node_client:query({Commands, #{table => Tablename}}).

%% Tablename::binary(), IteratorSetting::#iterator_setting{}, Scopes::list(majc|minc|scan)
attach_iterator(Tablename,IteratorSetting,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_ATTACH_ITERATOR),
	Options = #{table => Tablename, iterator_setting => IteratorSetting, scopes => Scopes},
	accumulo_node_client:query({Commands,Options}).
	
attach_iterator(Tablename,IteratorSetting) ->
	case maps:is_key(scopes,IteratorSetting) of
		false ->
			attach_iterator(Tablename,IteratorSetting,?SCOPES_ALL);
		true ->
			attach_iterator(Tablename,maps:remove(scopes,IteratorSetting,maps:get(scopes,IteratorSetting)))
	end.
%% {ok,attach_iterator}
%% {error,table_not_found | Error}

% -define(TBL_REMOVE_ITERATOR, 28).
remove_iterator(TableName,IteratorName,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_REMOVE_ITERATOR),
	Options = #{table => TableName, iterator_name => IteratorName, scopes => Scopes},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_GET_ITERATOR_SETTING, 29).
%% out : {ok,#iterator_setting{}} | {error,Message}
get_iterator_setting(TableName,IteratorName,Scope) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_GET_ITERATOR_SETTING),
	Options = #{table => TableName, iterator_name => IteratorName, scope => Scope},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_CHECK_ITERATOR_CONFLICTS, 31).
%% out : {ok,conflict|not_conflict} | {error,Message}
check_iterator_conflicts(Tablename,IteratorSetting,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_CHECK_ITERATOR_CONFLICTS),
	Options = #{table => Tablename, iterator_setting => IteratorSetting, scopes => Scopes},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_LIST_CONSTRAINTS, 34).
%% out : list(tuple(binary(),integer()))
list_constraints(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_LIST_CONSTRAINTS),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_ADD_CONSTRAINT, 32).
%% out : {ok,Constraint::integer()} | {error,Message}
add_constraint(TableName,ConstraintClass) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_ADD_CONSTRAINT),
	Options = #{table => TableName, class => ConstraintClass},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_REMOVE_CONSTRAINT, 33).
% in : TableName::binary(), Constraint::integer()
remove_constraint(TableName,Constraint) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_REMOVE_CONSTRAINT),
	Options = #{table => TableName, constraint => Constraint},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_LIST_SPLITS, 6).
% in : #table_split_query{table,max_splits}
%% out : list(binary())
list_splits(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_LIST_SPLITS),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands,Options}).
	
list_splits(TableName,MaxSplits) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_LIST_SPLITS),
	Options = #{table => TableName, max_splits => MaxSplits},
	accumulo_node_client:query({Commands,Options}).
		
% -define(TBL_ADD_SPLITS, 5).
% in : #table_split_query{table,partition_keys}
add_splits(TableName,PartitionKeys) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_ADD_SPLITS),
	Options = #{table => TableName, partition_keys => PartitionKeys},
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_SPLIT_RANGE_BY_TABLETS, 21).
% in : #table_split_query{table,range,max_splits}
split_range_by_tablets(TableName) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_SPLIT_RANGE_BY_TABLETS),
	Options = #{table => TableName},
	accumulo_node_client:query({Commands,Options}).

split_range_by_tablets(TableName,Range,MaxSplits) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_SPLIT_RANGE_BY_TABLETS),
	Options0 = #{table => TableName},
	Options1 = case Range of
		null ->
			Options0;
		_->
			maps:put(range,Range,Options0)
	end,
	
	Options = case MaxSplits of
		null ->
			Options1;
		_->
			maps:put(max_splits,MaxSplits,Options1)
	end,
	
	accumulo_node_client:query({Commands,Options}).

% -define(TBL_GET_DISK_USAGE, 35).
disk_usage(Tablenames) ->
	Commands = accumulo_util:gen_commands(?OP_TABLE,?TBL_GET_DISK_USAGE),
	accumulo_node_client:query({Commands, #{tables => Tablenames}}).

		
% -define(TBL_TEST_CLASS_LOAD, 36).
