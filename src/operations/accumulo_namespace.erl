-module(accumulo_namespace).
-include("accumulo.hrl").

-export([system_namespace/0,default_namespace/0]).
-export([list/0,exists/1,create/1, delete/1,rename/2]).
-export([set_property/2,remove_property/2,get_properties/1]).
-export([namespace_id_map/0]).
-export([attach_iterator/3,remove_iterator/3,get_iterator_setting/3,list_iterators/1,check_iterator_conflicts/3]).
-export([add_constraint/2,remove_constraint/2,list_constraint/1]).

% 
%% out : {ok, Namespace}
system_namespace() ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_SYSTEM_NAMESPACE),
	accumulo_node_client:query({Commands}).
	
%% out : {ok, Namespace}
default_namespace()->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_DEFAULT_NAMESPACE),
	accumulo_node_client:query({Commands}).
	
%% out : {ok, list(Namespace::binary())}
list() ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_LIST),
	accumulo_node_client:query({Commands}).

% in : Namespace::binary()
%% out : {ok,true}
exists(Namespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_EXISTS),
	accumulo_node_client:query({Commands, #{namespace => Namespace}}).

% in : Namespace::binary()
%% out : {ok,create} | {error,namespace_exists} | {error,Message}
create(Namespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_CREATE),
	accumulo_node_client:query({Commands, #{namespace => Namespace}}).


% in : Namespace::binary()
%% out : {ok,delete} 
%%			| {error,namespace_not_found} | {error,namespace_not_empty} 
%%			| {error,Message}
delete(Namespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_DELETE),
	accumulo_node_client:query({Commands, #{namespace => Namespace}}).
	
% in : Namespace::binary(), NewNamespace::binary()
%% out : {ok,rename} 
%%			| {error,namespace_not_found} | {error,namespace_exists} 
%%			| {error,Message}
rename(Namespace, NewNamespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_RENAME),
	accumulo_node_client:query({Commands, #{namespace => Namespace, new_namespace => NewNamespace}}).

% in : Namespace::binary(),tuple(Property::binary(),Value::binary())
%% out : {ok,set_property}
%%			| {error,namespace_not_found} 
%%			| {error,Message}
set_property(Namespace,{Property,Value}) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_SET_PROPERTY),
	Options = #{namespace => Namespace, property => {Property,Value}},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary(), Property::binary()
%% out : {ok,remove_property}
%%			| {error,namespace_not_found} 
%%			| {error,Message}
remove_property(Namespace, Property) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_REMOVE_PROPERTY),
	Options = #{namespace => Namespace, property =>Property},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary()
%% out : {ok,list({Property::binary(),Value::binary()})}
%%			| {error,namespace_not_found} 
%%			| {error,Message}
get_properties(Namespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_GET_PROPERTIES),
	Options = #{namespace => Namespace},
	accumulo_node_client:query({Commands,Options}).

% 
%% out : {ok,list(binary(),binary())}
namespace_id_map() ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_NAMESPACE_ID_MAP),
	accumulo_node_client:query({Commands}).

% in : Namespace::binary(), IteratorSetting::lists(tuple()),Scopes::list(atom())
%% out : {ok,attach_iterator} | {error,Message}
attach_iterator(Namespace,IteratorSetting,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_ATTACH_ITERATOR),
	Options = #{
		namespace => Namespace,
		iterator_setting => IteratorSetting,
		scopes => Scopes
	},
	accumulo_node_client:query({Commands,Options}).
	
% in : Namespace::binary(), IteratorName::binary(), Scopes::list(atom())
%% out : {ok,remove_iterator} | {error,Message}
remove_iterator(Namespace,IteratorName,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_REMOVE_ITERATOR),
	Options = #{
		namespace => Namespace,
		iterator_name => IteratorName,
		scopes => Scopes
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary(), IteratorName::binary(), Scopes::list(atom())
%% out : {ok,list(tuple())} | {error, Message}
get_iterator_setting(Namespace,IteratorName,Scope) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_GET_ITERATOR_SETTING),
	Options = #{
		namespace => Namespace,
		iterator_name => IteratorName,
		scope => Scope
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary()
%% out : {ok,list(list(tuple()))} | {error, Message}
list_iterators(Namespace) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_LIST_ITERATORS),
	Options = #{
		namespace => Namespace
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary(), IteratorSetting::lists(tuple()),Scopes::list(atom())
%% out : {ok, conflict | not_conflict} | {error, Message}
check_iterator_conflicts(Namespace,IteratorSetting,Scopes) ->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_CHECK_ITERATOR_CONFLICTS),
	Options = #{
		namespace => Namespace,
		iterator_setting => IteratorSetting,
		scopes => Scopes
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary(), Class::binary()
%% out : {ok, Constraint::integer()} | {error,Message}
add_constraint(Namespace,Class)->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_ADD_CONSTRAINT),
	Options = #{
		namespace => Namespace,
		class => Class
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary(), Constraint::integer()
%% out : {ok, remove_constraint} | {error, Message}
remove_constraint(Namespace,Constraint)->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_REMOVE_CONSTRAINT),
	Options = #{
		namespace => Namespace,
		constraint => Constraint
	},
	accumulo_node_client:query({Commands,Options}).

% in : Namespace::binary()
%% out : {ok, list(tuple(ConstraintName::binary(),ConstrantID::integer()))}
%%		| {error, Message}
list_constraint(Namespace)->
	Commands = accumulo_util:gen_commands(?OP_NAMESPACE,?NS_LIST_CONSTRAINTS),
	Options = #{
		namespace => Namespace
	},
	accumulo_node_client:query({Commands,Options}).
