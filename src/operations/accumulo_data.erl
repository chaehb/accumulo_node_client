-module(accumulo_data).

-include("accumulo.hrl").

-export([read/2]).
-export([write/2]).
-export([delete/2]).
-export([list/2]).
-export([search/2]).

%% Query::#entry_read_query{}
read(entry,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_READ,?DATA_READ_ENTRY),
	accumulo_node_client:query({Commands, Query});
		
read(batch,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_READ,?DATA_READ_BATCH),
	accumulo_node_client:query({Commands, Query});
		
read(_,_) ->
	{error,undefined}.
%% response
%%	{ok,true|false}
%%	{error, Error}

%%Query::#batch_write_query{}
write(batch, Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_WRITE,?DATA_WRITE_BATCH),
	accumulo_node_client:query({Commands,Query});
	
write(multi_table,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_WRITE,?DATA_WRITE_MULTI_TABLE),
	accumulo_node_client:query({Commands,Query});
	
write(conditional,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_WRITE,?DATA_WRITE_CONDITIONAL),
	accumulo_node_client:query({Commands,Query});
%% Result::list(Status::integer())

write(_,_)->
	{error,undefined}.

delete(batch, Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_DELETE,?DATA_DELETE_BATCH),
	accumulo_node_client:query({Commands,Query});
	
delete(search, Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_DELETE,?DATA_DELETE_SEARCH),
	accumulo_node_client:query({Commands,Query});
	
delete(_,_) ->
	{error,undefined}.
	
list(entries,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_LIST,?DATA_LIST_ENTRIES),
	accumulo_node_client:query({Commands,Query});

list(rows,Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_LIST,?DATA_LIST_ROWS),
	accumulo_node_client:query({Commands,Query});

list(_,_) ->
	{error,undefined}.

search(entries, Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_SEARCH, ?DATA_SEARCH_ENTRIES),
	accumulo_node_client:query({Commands,Query});

search(rows, Query) ->
	Commands = accumulo_util:gen_commands(?OP_DATA,?DATA_SEARCH, ?DATA_SEARCH_ROWS),
	accumulo_node_client:query({Commands,Query});

search(_,_) ->
	{error,undefined}.
