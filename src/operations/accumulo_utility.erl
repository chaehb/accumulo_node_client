-module(accumulo_utility).

-include("accumulo.hrl").

-export([monitor/0]).
-export([uuid/0]).
-export([whoami/0]).
-export([dashboard/0]).

monitor() ->
	Commands = accumulo_util:gen_commands(?OP_UTILITY,?UTIL_MONITOR),
	accumulo_node_client:query({Commands}).

uuid() ->
	Commands = accumulo_util:gen_commands(?OP_UTILITY,?UTIL_UUID),
	accumulo_node_client:query({Commands}).
whoami() ->
	Commands = accumulo_util:gen_commands(?OP_UTILITY,?UTIL_WHOAMI),
	accumulo_node_client:query({Commands}).

dashboard() ->
	Commands = accumulo_util:gen_commands(?OP_UTILITY,?UTIL_DASHBOARD),
	accumulo_node_client:query({Commands}).
