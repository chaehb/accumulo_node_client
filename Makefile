REBAR = /usr/local/bin/rebar

all:
	@$(REBAR) compile

clean:
	@$(REBAR) clean
	rm -rf ebin
